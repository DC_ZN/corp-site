// app/routes.js
var svgCaptcha = require('svg-captcha');
var dbconfig = require('../config/database');
var bcrypt = require('bcrypt-nodejs');
var request = require('request');
var proxy = require('express-http-proxy');
const fs = require('fs');
const path = require('path');
var Busboy = require('busboy');
var inspect = require('util').inspect;
var crypto = require('crypto');

// for capcha
const options = {
    size: 4,
    ignoreChars: '0o1i',
    noise: 4,
    color: true,
    background: '#fff',
    width:145
};

const ETH_RPC_SERVER = 'http://ethrpc:3000';

// News
var priv_news_config = require('../config/private_news');
var pub_news_config = require('../config/news');
var news_number = Object.keys(pub_news_config).length;
var priv_news_number = Object.keys(priv_news_config).length;

// Quests
const quests_conf =  require('../config/quests');

const const_sett = require('../config/const_settings');
var sha256 = require('js-sha256');
var sha256_img_elephant = const_sett.sha256_of_elephant_img;

const slide_ads = const_sett.slide_show_ads;
const vk_password = const_sett.vk_password;
const qinit_token = const_sett.qinit_token;
const qinit_addr = const_sett.qinit_addr;
const supervisor_addr = const_sett.supervisor_addr;
const check_status_interval = const_sett.interval_of_check_status*1000; // seconds to mSeconds

const true_sha512 = "9120CD5FAEF07A08E971FF024A3FCBEA1E3A6B44142A6D82CA28C6C42E4F852595BCF53D81D776F10541045ABDB7C37950629415D0DC66C8D86C64A5606D32DE";
const false_sha512 = "719FA67EEF49C4B2A2B83F0C62BDDD88C106AAADB7E21AE057C8802B700E36F81FE3F144812D8B05D66DC663D908B25645E153262CF6D457AA34E684AF9E328D";

// Bot
var api = require('../config/chat_bot/api');
var md5 = require('md5');
var first_msg_conf = require('../config/first_message');
const USER_SALT = const_sett.salt_for_socket_io_session;

var map_user_docker = new Map();
var map_user_filenames = new Map();

// dirPath: target image directory
function getImagesFromDir(dirPath) {
    // All iamges holder, defalut value is empty
    let allImages = [];
    // Iterator over the directory
    let files = fs.readdirSync(dirPath);
    // Iterator over the files and push jpg and png images to allImages array.
    for (file of files) {
        let fileLocation = path.join(dirPath, file);
        var stat = fs.statSync(fileLocation);
        if (stat && stat.isDirectory()) {
            getImagesFromDir(fileLocation); // process sub directories
        } else if (stat && stat.isFile() && ['.jpg', '.png'].indexOf(path.extname(fileLocation)) != -1) {
            allImages.push('static/'+file); // push all .jpf and .png files to all images 
        }
    }
    // return all images in array formate
    return allImages;
}

const images_list = getImagesFromDir(path.join(__dirname, '../public/images/ads'));

function parse_word(text, word){
        var re1 = new RegExp( "[\s\S]*" + word + "[ .,!?ау]+[\s\S]*");
        var re2 = new RegExp( "[\s\S]*" + word + "[ .,!?ау]*$");
        return re1.test(text.toLowerCase()) || re2.test(text.toLowerCase());
}
function parse_hex_quest5(text, word){
	var re1 = new RegExp( "[\s\S]*" + word + "[ .,!?]+[\s\S]*");
	var re2 = new RegExp( "[\s\S]*" + word + "[ .,!?]*$");
	return re1.test(text.toLowerCase()) || re2.test(text.toLowerCase());
}
	
module.exports = function(app, passport, connection, sessionMiddleware) {
	var soc_map = new Map();
	app.io.use(function(b_socket, next) {
        sessionMiddleware(b_socket.request, b_socket.request.res, next);
    });

    connection.query('USE ' + dbconfig.database);
    
    function update_top_table(username, quest_numb, points){
        console.log("[CHAT BOT] (quest_" + quest_numb + ") req to update TOP_QUESTS: start; user: " + username);
        var date = new Date();
        var current_datetime = '' + date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
        connection.query("UPDATE " + dbconfig.quests_top_table + " SET quests_numb=" + quest_numb + ",points=" + points.toString() + ",datetime='" + current_datetime +"' WHERE username='"+username+"'", function (err, rows) {
            if(!err) console.log("[CHAT BOT] (quest_" + quest_numb + ") req to update TOP_QUESTS: finished; user: " + username);
            else console.log("[CHAT BOT] (quest_" + quest_numb + ") req to update TOP_QUESTS: ERROR; user: " + username + " " + err);
        });
    }
	
	// =======================================================
	console.log("[START SERVICE] Get all users from DB - start");
	connection.query("SELECT username FROM " + dbconfig.users_table , function (err, usernames) {
            if (!err) {
				console.log("[START SERVICE] Get all users from DB - finished");
				usernames.forEach(function(row, i, usernames) {
					connection.query("SELECT dockerID,filename4,filename5 FROM " + dbconfig.user_info_quest_table + " WHERE username='"+row.username+"'", function (err, rows) {
						if (!err) {
							if(rows[0].dockerID) {
								map_user_docker.set(row.username, rows[0].dockerID);
							} else {
								map_user_docker.set(row.username, -1);
							}
							if(rows[0].filename4) {
								let filenames = map_user_filenames.get(row.username);
								if (filenames === undefined) {
									console.log("filenames undefined");
								  filenames = new Object();
								}
								filenames.quest_4 = rows[0].filename4
								map_user_filenames.set(row.username, filenames);
								console.log("Put filenames.quest_4="+filenames.quest_4);
							}

							if(rows[0].filename5) {
								let filenames = map_user_filenames.get(row.username);
								if (filenames === undefined) {
								  filenames = new Object();
								}
								filenames.quest_5 = rows[0].filename5
								map_user_filenames.set(row.username, filenames);
							}
						} else {
							console.log("[START SERVICE] ERROR when get docker_id,filename4,filename5 for user '"+row.username+"': " + err);
						}
					});
				});
				
				setInterval(function check_status() {
					map_user_docker.forEach( (docker_id, username, map) => {
						try {
							if(docker_id !== -1){
								console.log("Req for user: " + username + " and docker ID " + docker_id);
								request('http://supervisor:3000/v2/onions/' + docker_id, function (error, response, body) {
									if (!error && response.statusCode === 200) {
										console.log("Response from supervisor: " + body);
										var res_ = JSON.parse(body);
										if(res_.quests && res_.quests.length > 0){
											connection.query("SELECT quests_news FROM " + dbconfig.user_info_quest_table + " WHERE username='"+username+"'", function (err, rows) {
												if (!err) {
													var quests_obj = JSON.parse(rows[0].quests_news); // to object
													for(var i in res_.quests){
														let q_name = res_.quests[i].name;
														if(quests_obj.quests[q_name]) {
															let need_update = false;
															if(parseInt(res_.quests[i].current_progress) > parseInt(quests_obj.quests[q_name].current_progress)){
																need_update = true;
																quests_obj.quests[q_name].current_progress = res_.quests[i].current_progress;
																var u_obj = soc_map.get(username);
																if(u_obj && u_obj.cur_quest === q_name) {
																	if(quests_obj.quests[q_name].current_progress === quests_obj.quests[q_name].progress_max){
																		console.log('!!! Supervisor says, that user "'+username+'" finished quest "'+q_name+'" !!!');
																		u_obj.current_progress = 0;
																		u_obj = send_hello("", quests_obj, u_obj, true);
																		
																		if(quests_obj.news["private_news_" + (q_name.substr(q_name.length - 1))])
																			quests_obj.news["private_news_" + (q_name.substr(q_name.length - 1))] = 1;
                                                                        else console.log("[ERROR] " + "private_news_" + (q_name.substr(q_name.length - 1)) + 'not found');
																	}
																	else{
																		u_obj.current_progress = quests_obj.quests[q_name].current_progress;
																	}
																	soc_map.set(username, u_obj);
																}
																// Should be correct in the config file
																// quests_obj.quests[q_name].progress_max = res_.quests[i].progress_max;
															}

															// if we need to update something
															if( (res_.quests[i].hostname && (quests_obj.quests[q_name].hostname !== res_.quests[i].hostname)) ||
															    (res_.quests[i].ip && (quests_obj.quests[q_name].ip !== res_.quests[i].ip)) ||
																(res_.quests[i].password && (quests_obj.quests[q_name].password !== res_.quests[i].password)) ){
                                                                
                                                                // We need to send request to Qinit once we get onionweb hostname from quest_4
																if(q_name === "quest_4" && quests_obj.quests[q_name].hostname !== res_.quests[i].hostname) {
																	console.log("Current quest_4 hostname=" + quests_obj.quests[q_name].hostname + ", new hostname=" + res_.quests[i].hostname);
																	qinit_req4(username, res_.quests[i].hostname);
																}
																if(q_name === "quest_5" && quests_obj.quests[q_name].hostname !== res_.quests[i].hostname) {
																	console.log("Current quest_5 hostname=" + quests_obj.quests[q_name].hostname + ", new hostname=" + res_.quests[i].hostname);
																	qinit_req5(username, res_.quests[i].hostname);
																}
                                                                
                                                                need_update = true;
                                                                quests_obj.quests[q_name].hostname = res_.quests[i].hostname;
																quests_obj.quests[q_name].ip = res_.quests[i].ip;
																quests_obj.quests[q_name].password = res_.quests[i].password;
															}
															if(need_update){
																var fin_res = JSON.stringify(quests_obj); // return to string
																// Update
																console.log("[SUPER API] Trying to update DB for user '" + username + "'");
																connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET quests_news='"+fin_res+"' WHERE username='"+username+"'",function (err, rows) {
																	if (err) {
																		console.log("[SUPER API] Error when UPDATE DB for user '" + username + "': " + err);
																	}
																	else  console.log("[SUPER API] UPDATE DB for user '" + username + "': SUCCESS");
																});
                                                                
                                                                let points_ = quests_obj.quests[q_name].current_progress / quests_obj.quests[q_name].progress_max;
                                                                if(points_ >= 1) points_ = 0.0;
																if(typeof u_obj === "undefined") {
																	let u_obj = soc_map.get(username);
																	if(typeof u_obj !== "undefined")
																		update_top_table(username, (parseInt(u_obj.cur_quest[u_obj.cur_quest.length - 1]) - 1).toString(), Math.round(points_ * 10) / 10);
																}
																else update_top_table(username, (parseInt(u_obj.cur_quest[u_obj.cur_quest.length - 1]) - 1).toString(), Math.round(points_ * 10) / 10);
																	
															}
														}
														else console.log("[SUPER API] quest '"+q_name+"' don't known");
													}
												}
												else{ console.log("[SUPER API] ERROR when get quest_table for user '"+row.username+"': " + err); }
											});
										}
									}
								});
							}
							else{
								var u_obj = soc_map.get(username);
								if(u_obj && u_obj.cur_quest !== "quest_1") api_1(username);
							}
						}
						catch(e) {
							console.log('Error with api: ' + e.name + ":" + e.message + "\n" + e.stack);
						}
					});
				}, check_status_interval);
            }
			else console.log("ERROR when get users from DB: " + err);
        });
	// =======================================================

    // =====================================
    // API 1  ================================
    // =====================================

    function api_1(username){
        // Configure the request
        var options = {
            url: supervisor_addr,
            method: 'POST',
            json: { service_name: username }
        };
        // запрос на получение докерID
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200 && body.id) {
                // {"id":"id1|id2"}
                // Update
                console.log("Data from supervisor: " + body);
				map_user_docker.set(username, body.id);
				console.log("[API 1] Trying to update db for user '" + username + "'");
                connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET dockerID='"+body.id+"' WHERE username='"+username+"'",function (err, rows) {
					if (err) {
						console.log("[API 1] Error when UPDATE DB for user '" + username + "': " + err);
					}
					else  console.log("[API 1] UPDATE DB for user '" + username + "': SUCCESS");
				});
            }
            else console.log("[API 1] ERROR! There is something error with post req to supervisor: " + error);
        });
    }
    
    // =====================================
    // QINIT  ==============================
    // =====================================

    function qinit_req4(username, hostname){
        // Configure the request
        var options = {
            url: qinit_addr,
            method: 'POST',
            json: { token: qinit_token, username: username, quest_name: "encrypted_disk", quest_inputs: { password: vk_password, onion: hostname} }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200 && body.quest_outputs.filename) {
                // {quest_outputs:{filename: "a6a7b78c-bd81-4991-af61-50c5a77cb812.raw.tar.xz"}}
                console.log("Data from QINIT: " + JSON.stringify(body));
                let filenames = map_user_filenames.get(username);
                if (filenames === undefined) {
									filenames = new Object();
                }
                filenames.quest_4 = body.quest_outputs.filename;
                map_user_filenames.set(username, filenames);
                console.log("[QINIT] Trying to update db for user '" + username + "'");
                connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET filename4='"+body.quest_outputs.filename+"' WHERE username='"+username+"'",function (err, rows) {
                    if (err) {
                        console.log("[QINIT] Error when UPDATE DB for user '" + username + "': " + err);
                    }
                    else  console.log("[QINIT] UPDATE DB for user '" + username + "': SUCCESS");
                });
            }
            else console.log("[QINIT] ERROR! There is an error with post req to QINIT: " + error);
        });
    }

    function qinit_req5(username, hostname){
        // Configure the request
        var options = {
            url: qinit_addr,
            method: 'POST',
            json: { token: qinit_token, username: username, quest_name: "backdoored_apk", quest_inputs: { onion: hostname }}
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200 && body.quest_outputs.filename) {
                // {quest_outputs:{filename: "a6a7b78c-bd81-4991-af61-50c5a77cb812.apk"}}
                console.log("Data from QINIT: " + JSON.stringify(body));
                let filenames = map_user_filenames.get(username);
                if (filenames === undefined) {
									filenames = new Object();
                }
                filenames.quest_5 = body.quest_outputs.filename;
                map_user_filenames.set(username, filenames);
                console.log("[QINIT] Trying to update db for user '" + username + "'");
                connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET filename5='"+body.quest_outputs.filename+"' WHERE username='"+username+"'",function (err, rows) {
                    if (err) {
                        console.log("[QINIT] Error when UPDATE DB for user '" + username + "': " + err);
                    }
                    else  console.log("[QINIT] UPDATE DB for user '" + username + "': SUCCESS");
                });
            }
            else console.log("[QINIT] ERROR! There is an error with post req to QINIT: " + error);
        });
    }


    // =====================================
    // HOME PAGE  ==========================
    // =====================================
    app.get('/', function(req, res) {
        if(req.isAuthenticated()){ // check other table, specific news for user
			if(!map_user_docker.has(req.user.username)) map_user_docker.set(req.user.username, -1);
		
            req.session.links_flag = 1;
            req.session.username__ = req.user.username;
            if (typeof req.session.new_message === "undefined") req.session.new_message = true;
            if (typeof req.session.show_elephant === "undefined" || typeof req.session.ads_location === "undefined" || typeof req.session.slide_ads === "undefined"){
                req.session.show_elephant = false;  // cause req bellow take some time
                req.session.ads_location = "right"; // cause req bellow take some time
                check_quest(req, res, 'quest_2', function(req, res, is_quest_2, old_quests){
                    req.session.show_elephant = is_quest_2; // if quest_2 is not completed
                    req.session.ads_location = old_quests.ads_location || req.session.ads_location;
                    req.session.slide_ads = old_quests.slide_ads || slide_ads;
                });
            }

            // Set cookie
            let cookie = req.cookies.moderator;
			cookie = cookie ? cookie.toUpperCase() : false;
            if (cookie === false || (cookie !== false_sha512 && cookie !== true_sha512)) {
                // Set a new cookie
                res.cookie('moderator', false_sha512, { maxAge: 900000, httpOnly: true });
            } 
            connection.query("SELECT quests_news FROM " + dbconfig.user_info_quest_table + " WHERE username='"+req.user.username+"'", function (err, rows) {
                if (!err && rows.length) {
                    var news = JSON.parse(rows[0].quests_news).news; // get list of news, avaible for the user
                    connection.query("SELECT news_prev,news_name FROM " + dbconfig.news_table + " ORDER BY news_name DESC", function (err, rows) {
                        res.render('index.ejs', {   new_mess:req.session.new_message,
                                                    data: rows, 
                                                    news: news, 
                                                    showLogout : req.session.links_flag,
                                                    isAdmin: is_admin(req), 
                                                    show_ads:true, 
                                                    images: image_filter(req), 
                                                    ads_location:req.session.ads_location,
                                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                                }); // load the index.ejs file
                    });
                }
            });
        }
        else { // check public table
            connection.query("SELECT news FROM " + dbconfig.public_news_table, function (err, rows) {
                var news = JSON.parse(rows[0].news); // news for everyone
                req.session.links_flag = 0;
                connection.query("SELECT news_prev,news_name FROM " + dbconfig.news_table + " ORDER BY news_name DESC", function (err, rows) {
                    res.render('index.ejs', {   new_mess:req.session.new_message,
                                                data: rows, 
                                                news: news, 
                                                showLogout : req.session.links_flag, 
                                                isAdmin: is_admin(req), 
                                                show_ads:true, 
                                                images: image_filter(req), 
                                                ads_location:req.session.ads_location,
                                                slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                            }); // load the index.ejs file
                });
            });
        }
    });

    // =====================================
    // LOGIN  ===============================
    // =====================================

    // show the login form
    app.get('/login', function(req, res) {
        // render the page and pass in any flash data if it exists
        var captcha = svgCaptcha.create(options);
        req.session.captcha = captcha.text;
        // render the page and pass in any flash data if it exists
        res.render('login.ejs', {   new_mess:req.session.new_message,
                                    message: req.flash('loginMessage'),
                                    cap: captcha.data,
                                    showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    show_ads:true,
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                });
    });

    // process the login form
    app.post('/login', passport.authenticate('local-login', {
            successRedirect : '/', // redirect to the secure profile section
            failureRedirect : '/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }),
        function(req, res) {
            if (req.body.remember) {
                req.session.cookie.maxAge = 1000 * 60 * 3;
            } else {
                req.session.cookie.expires = false;
            }
            res.redirect('/');
        });

    // =====================================
    // SIGNUP  ==============================
    // =====================================

    // show the signup form
    app.get('/signup', function(req, res) {
        // render the page and pass in any flash data if it exists
        var captcha = svgCaptcha.create(options);
        req.session.captcha = captcha.text;
        res.render('signup.ejs', {  new_mess:req.session.new_message,
                                    message: req.flash('signupMessage'),
                                    cap: captcha.data,
                                    showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req) ,
                                    show_ads:true,
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                });
    });

    // process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/', // redirect to the secure profile section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    // =====================================
    // LOGOUT ==============================
    // =====================================

    app.get('/logout', function (req, res) {
        if(req.isAuthenticated()) {
            req.session.links_flag = 0;
            req.logout();
            res.redirect('/');
        }
        else res.redirect('/');
    });

    // =====================================
    // MARKET ==============================
    // =====================================

    app.get('/market', function (req, res) {
        res.status(503);
        res.render('error503', {    new_mess:req.session.new_message,
                                    showLogout : req.session.links_flag, 
                                    isAdmin: is_admin(req)
                                });
    });

    // =====================================
    // SECYRITY FORM =======================
    // =====================================

    app.get('/report_page', function (req, res) {
        if(req.isAuthenticated()){
            res.status(200);
            var captcha = svgCaptcha.create(options);
            req.session.captcha = captcha.text;
            res.render('security_form', {   new_mess:req.session.new_message,
                                                    showLogout : req.session.links_flag,
                                                    isAdmin: is_admin(req), 
                                                    cap: captcha.data,
                                                    images: image_filter(req, true),
                                                    ads_location:req.session.ads_location,
                                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                                });
        }
        else{
            res.status(404);
            res.render('error404',{ new_mess:req.session.new_message,
                                    showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });

    // =====================================
    // NEWS FOR EVERYONE ===================
    // =====================================

    app.get('/news_[1-' + news_number + ']', function(req, res) {
        var url_ = req.url.slice(1);
        connection.query("SELECT news_text FROM " + dbconfig.news_table + " WHERE news_name='"+url_+"'", function (err, rows) {
            if (!err){
                res.render('post.ejs', { new_mess:req.session.new_message,
                                        data: rows ,
                                        showLogout : req.session.links_flag,
                                        isAdmin: is_admin(req),
                                        show_ads:true,
                                        images: image_filter(req),
                                        ads_location:req.session.ads_location,
                                        slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                       });
            }
        });
    });

    // =====================================
    // PERSONAL NEWS =======================
    // =====================================

    app.get('/private_news_[1-' + priv_news_number + ']', function(req, res) {
        if(req.isAuthenticated()){
            connection.query("SELECT quests_news FROM " + dbconfig.user_info_quest_table + " WHERE username='"+req.user.username+"'", function (err, rows) {
                var news = JSON.parse(rows[0].quests_news).news; // rewrite news for the user
                var url_ = req.url.slice(1);
                if(news[url_] && news[url_] !== -1){
                    connection.query("SELECT news_text FROM " + dbconfig.news_table + " WHERE news_name='"+url_+"'", function (err, rows) {
                        res.render('post.ejs', { new_mess:req.session.new_message,
                                                data: rows ,
                                                showLogout : req.session.links_flag,
                                                isAdmin: is_admin(req),
                                                show_ads:true,
                                                images: image_filter(req),
                                                ads_location:req.session.ads_location,
                                                slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                               });
                    });
                }
                else {
                    res.status(404);
                    res.render('error404',{ snew_mess:req.session.new_message,
                                            howLogout : req.session.links_flag,
                                            isAdmin: is_admin(req),
                                            images: image_filter(req),
                                            ads_location:req.session.ads_location,
                                            slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                        });
                }
            });
        }
       else {
            res.status(404);
            res.render('error404',{ new_mess:req.session.new_message,
                                    showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });

    // =====================================
    // ADS ADMIN  ==========================
    // =====================================

    function image_filter(req, custom_slide_ads){
        show_elephant = req.session.show_elephant || false;
        let images = images_list.slice();
        if(!show_elephant){
            // remove elephant
            var i_arr = [];
            var i_arr_count = 0;
            for (var i in images){
                if(images[i].indexOf("bishop") > 0) {
                    i_arr[i_arr_count] = i;
                    i_arr_count++;
                }
            }
            for(var i in i_arr)  images.splice(i_arr[i], 1);
        }
        custom_slide_ads = custom_slide_ads || (typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads);
        if(custom_slide_ads) return images;
        return [images[Math.floor(Math.random()*images.length)]];
    }

    app.get('/ads_admin', function(req, res) {
        var captcha = svgCaptcha.create(options);
        if(req.isAuthenticated()){
            var u_obj = soc_map.get(req.user.username);
            if(u_obj && u_obj.cur_quest === "quest_2") {
                connection.query("SELECT quests_news FROM " + dbconfig.user_info_quest_table + " WHERE username='"+req.user.username+"'", function (err, rows) {

                    var quests_obj = JSON.parse(rows[0].quests_news); // to object

                    req.session.captcha = captcha.text;
                    let cookie = req.cookies.moderator;
                    cookie = cookie ? cookie.toUpperCase() : false;

                    if (cookie && cookie === true_sha512)
                    {
                        if(quests_obj.quests.quest_2.current_progress === 1) {
                            quests_obj.quests.quest_2.current_progress = 2;
                            var fin_res = JSON.stringify(quests_obj); // return to string
                            // Update
                            connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET quests_news='"+fin_res+"' WHERE username='"+req.user.username+"'",function (err, rows) {
                                    if (err) {
                                        console.log("Error when UPDATE DB (quest_2.current_progress = 2): " + err);
                                    }
                                    else  console.log("UPDATE DB (quest_2.current_progress = 2): SUCCESS; user: " + req.user.username);
                                });
                            
                            var u_obj = soc_map.get(req.user.username);
                            u_obj.current_progress = 2;
                            soc_map.set(req.user.username, u_obj);
                            
                            console.log('User "'+req.user.username+'" found the "/ads_admin" page; Cookie = true;');
                            
                            let points_ = quests_obj.quests.quest_2.current_progress / quests_obj.quests.quest_2.progress_max;
                            update_top_table(req.user.username, "1", Math.round(points_ * 10) / 10); // 1 = number of finished quests
                        }
                        

                        res.render('ads_admin.ejs', {   new_mess:req.session.new_message,
                                                        showLogout : req.session.links_flag,
                                                        isAdmin: is_admin(req), 
                                                        cap: captcha.data,
                                                        images: image_filter(req, true),
                                                        ads_location:req.session.ads_location,
                                                        slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                                    });
                    } 
                    else {
                        if(quests_obj.quests.quest_2.current_progress === 0) {
                            quests_obj.quests.quest_2.current_progress = 1;
                            var fin_res = JSON.stringify(quests_obj); // return to string
                            // Update
                            connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET quests_news='"+fin_res+"' WHERE username='"+req.user.username+"'",function (err, rows) {
                                    if (err) {
                                        console.log("Error when UPDATE DB (quest_2.current_progress = 1): " + err);
                                    }
                                    else  console.log("UPDATE DB (quest_2.current_progress = 1): SUCCESS; user: " + req.user.username);
                                });
                            
                            var u_obj = soc_map.get(req.user.username);
                            u_obj.current_progress = 1;
                            soc_map.set(req.user.username, u_obj);
                            
                            console.log('User "'+req.user.username+'" found the "/ads_admin" page; Cookie = false;');

                            let points_ = quests_obj.quests.quest_2.current_progress / quests_obj.quests.quest_2.progress_max;
                            update_top_table(req.user.username, "1", Math.round(points_ * 10) / 10); // 1 = number of finished quests
                        }
                        res.status(403);
                        res.render('error403',{ new_mess:req.session.new_message,
                                                showLogout : req.session.links_flag,
                                                isAdmin: is_admin(req),
                                                cap: captcha.data,
                                                images: image_filter(req),
                                                ads_location:req.session.ads_location,
                                                slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                            });
                    }
                });
            }
            else {
                res.status(404);
                res.render('error404',{ new_mess:req.session.new_message,
                                        showLogout : req.session.links_flag,
                                        isAdmin: is_admin(req),
                                        images: image_filter(req),
                                        ads_location:req.session.ads_location,
                                        slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                      });
            }
        }
        else {
            res.status(404);
            res.render('error404',{ new_mess:req.session.new_message,
                                    showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });

    app.post('/ads_admin', function(req, res) {
        if(req.isAuthenticated()){
            var captcha = svgCaptcha.create(options);

            let cap = "";
            let sha256_img = "";
            let img_filename = "";
            let img_length = 0;
            let ads_location = "";
            let is_slide = "";
            if (req.busboy) {
                req.busboy.on('file', function (fieldname, file, filename) {
                    console.log("[" + req.user.username + "] File" + filename +" is loading via ads_admin form...");
                    var hash = crypto.createHash('sha256');

                    file.on('data', function(data) {
                        hash.update(data);
                        img_filename = filename;
                        img_length = data.length;
                    });
                    file.on('end', function() {
                        sha256_img = hash.digest('hex');
                    });
                });
                req.busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated) {
                    console.log("User [" + req.user.username + '] send: field [' + fieldname + ']: value: ' + inspect(val));
                    switch(fieldname) {
                        case 'captcha':
                            cap = inspect(val).replace("'","").replace("'","");
                            break;

                        case 'location':
                            ads_location = inspect(val).replace("'","").replace("'","");
                            break;
                        case 'isSlide':
                            is_slide = inspect(val).replace("'","").replace("'","");
                            break;

                        default:
                            break;
                    }
                });
                req.busboy.on('finish', function() {
                    let message = "";
                    let suc_message = "";
                    if(req.session.captcha === cap){
                        req.session.captcha = captcha.text;
                        if(img_filename === "" || img_filename.substring(img_filename.length-4) === ".png"|| img_filename.substring(img_filename.length-4) === ".jpg"){
                            // OK
                            check_quest(req, res, 'quest_2', function(req, res, is_quest_2, old_quests){
                                if(img_length > 0) { // There is image
                                    console.log("User [" + req.user.username + "] Sha256 of uploaded image is: " + sha256_img);
                                    if(sha256_img === sha256_img_elephant || sha256_img === "7ea1242c06c6094d8a97d9a8051793046c782c8cef36ddfbfff1a979b0637f6b"){
                                        if(is_quest_2){
                                            send_res(res, req, 'Кто тут хороший мальчик? Ты уже прошел этот квест =)',"",captcha);
                                        }
                                        else{
                                            // Update DB
                                            if(check_fields(ads_location, is_slide, req, res, captcha, old_quests)){
                                                old_quests.quests.quest_2.current_progress = old_quests.quests.quest_2.progress_max;

												if(old_quests.news.private_news_2) old_quests.news.private_news_2 = 1;
                                                var fin_res = JSON.stringify(old_quests); // return to string
                                                // Update
                                                connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET quests_news='"+fin_res+"' WHERE username='"+req.user.username+"'");
                                                req.session.show_elephant = true;
                                                req.session.new_message = true;
                                                console.log("+++++++++++++++++++++++++++++\nUser " + req.user.username + " finished quest_2! \n+++++++++++++++++++++++++++++");

                                                update_top_table(req.user.username, "2", 0.0); // 2 = number of finished quests
                                                
                                                var u_obj = soc_map.get(req.user.username);
												u_obj.current_progress = 0;
                                                u_obj = send_hello("", old_quests, u_obj, true);
												soc_map.set(req.user.username, u_obj);
                                                
                                                send_res(res, req, 'Сохранено.',"",captcha);
                                            }
                                            else {send_res(res, req, '',"Что-то не так с данными...",captcha);}
                                        }
                                    }
                                    else {send_res(res, req, '',"С изображением что-то не так...",captcha);}
                                }
                                else{ 
                                    if(check_fields(ads_location, is_slide, req, res, captcha, old_quests)){
                                        // Update
                                        var fin_res = JSON.stringify(old_quests); // return to string
                                        connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET quests_news='"+fin_res+"' WHERE username='"+req.user.username+"'");
                                        send_res(res, req, 'Сохранено.',"",captcha);
                                    }
                                    else {send_res(res, req, '',"Что-то не так с данными...",captcha);}
                                }
                            });
                        }
                        else{ send_res(res, req, '',"Допустимые расширения: .png и .jpg",captcha);}
                    }
                    else{ send_res(res, req, '',"Проверьте капчу!",captcha);}
                });
                req.pipe(req.busboy);
            }
            else{
                send_res(res, req, '','',captcha);
                console.log("Something bad with busboy =(");
            }
        }
        else {
            res.status(404);
            res.render('error404',{    showLogout : req.session.links_flag,
                                        isAdmin: is_admin(req),
                                        images: image_filter(req, true),
                                        ads_location:req.session.ads_location,
                                        slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                    });
        }
    });

    function send_res(res, req, good_msg, bad_msg, captcha){
        res.status(200);
        req.session.captcha = captcha.text;
        res.render('ads_admin',{    new_mess:req.session.new_message,
                                    showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    cap: captcha.data,
                                    images: image_filter(req, true),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads,
                                    message: bad_msg,
                                    isSuccess: good_msg
                                });
    }

    function check_fields(ads_location,is_slide, req, res, captcha, old_quests){
        switch(ads_location) {
            case 'Справа':
                req.session.ads_location = "right";
                break;
            case 'Слева':
                req.session.ads_location = "left";
                break;
            default:
                return 0;
        }

        switch(is_slide) {
            case 'При перезагрузке страницы':
                req.session.slide_ads = false;
                break;
            case 'Каждые три секунды':
                req.session.slide_ads = true;
                break;
            default:
                return 0;
        }
        old_quests.ads_location = req.session.ads_location;
        old_quests.slide_ads = req.session.slide_ads;
        return 1;
    }

    function check_quest(req, res, quest_number, callback){
        connection.query("SELECT quests_news FROM " + dbconfig.user_info_quest_table + " WHERE username='"+req.user.username+"'", function (err, rows) {
            let old_quests = JSON.parse(rows[0].quests_news); // to object
            if(old_quests.quests[quest_number].current_progress === old_quests.quests[quest_number].progress_max) callback(req, res, 1, old_quests);
            else callback(req, res, 0, old_quests);
        });
    }
    // =====================================
    // API FOR TOP TABLE ===================
    // =====================================

    app.get('/70p_u53r5_74bl3', function(req, res) {
        if(req.query.param == "I7_I5_me_g37_t0p_pl5"){
            connection.query("SELECT * FROM " + dbconfig.user_info_quest_table , function (err, rows) {
                if (err) {
                    // Configure the request
                    res.status(500).json({});
                }
                else {
                    json_str = {};
                    rows.forEach(function(row){
                        json_str[row.username] = JSON.parse(row.quests_news).quests;
                    });
                    res.status(200).json({secret:"h3r3__y0u__g0", data:json_str});
                }
            });
        }
        else {
            res.status(404);
            res.render('error404',{ showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });
    
    // =====================================
    // NEW API FOR TOP TABLE ===============
    // =====================================
    
    app.get('/NEW_70p_u53r5_74bl3', function(req, res) {
        if(req.query.param == "NEW_I7_I5_me_g37_t0p_pl5"){
            console.log("[API TOP TABLE] req to bd: start");
            connection.query("SELECT * FROM " + dbconfig.quests_top_table + " ORDER BY quests_numb DESC,points DESC,datetime", function (err, rows) {
                if (err) {
                    console.log("[API TOP TABLE] req to bd: error = " + err);
                    res.status(500).json({});
                }
                else {
                    console.log("[API TOP TABLE] req to bd: success");
                    json_str = {};
                    rows.forEach(function(row){
                        json_str[row.username + " "] = {
                                                    quests_numb: row.quests_numb,
                                                    points: row.points
                                                 };
                    });
                    res.status(200).json({secret:"h3r3__y0u__g0", data:json_str});
                }
            });
        }
        else {
            res.status(404);
            res.render('error404',{ showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });

    // =====================================
    // PERSONAL PAGE =======================
    // =====================================

    app.use('/loan', proxy(ETH_RPC_SERVER + '/loan', {
        filter: function (req, res) {
            return req.isAuthenticated() && req.body.captcha && req.body.captcha === req.session.captcha;
        },
        parseReqBody: false,
        timeout: 10000,
        proxyReqPathResolver: function (req) {
            return '/loan';
        },
        userResDecorator: function (proxyRes, proxyResData, userReq, userRes) {
            var data = JSON.parse(proxyResData.toString('utf8'));
            console.log('/loan response ', data);
            if (data.address) {
                // Save user's address
                connection.query("UPDATE " + dbconfig.users_table + " SET address=? WHERE username=?", [data.address, userReq.user.username], function (err, rows) {
                    console.log('updated ' + userReq.user.username + ' address to ' + data.address);
                });
            }

            userRes.status(303);
            userRes.set('content-type', 'text/plain');

            if (data.ok) {
                userRes.set('location', '/your_page?success=true');
                return 'success';
            } else {
                userRes.set('location', '/your_page?error=true');
                return data.error;
            }
        }
    }))

    function check_quest_4(req, callback){
        connection.query("SELECT quests_news FROM " + dbconfig.user_info_quest_table + " WHERE username='"+req.user.username+"'", function (err, rows) {
            var quests = JSON.parse(rows[0].quests_news).quests; // to object
            quests.quest_4.current_progress === quests.quest_4.progress_max ? callback(true) : callback(false);
        });
    }

    app.get('/your_page', function (req, res) {
        if(req.isAuthenticated()){
            check_quest_4(req, function(result){
                if(result) { // if user complete third quests
                    var resSent = false;
                    var captcha = svgCaptcha.create(options);
                    req.session.captcha = captcha.text;

                    function sendResponse(balances) {
                        if (resSent) {
                            return;
                        }
                        res.render('profile', { new_mess:req.session.new_message,
                                                uname: req.user.username, 
                                                cap: captcha.data,
                                                address: req.user.address,
                                                balances: balances,
                                                isSuccess: req.query.success,
                                                isError: req.query.error,
                                                showLogout : req.session.links_flag, 
                                                isAdmin: is_admin(req), 
                                                show_ads:true, 
                                                images: image_filter(req), 
                                                ads_location:req.session.ads_location,
                                                slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                              });
                        resSent = true;
                    }

                    if (req.user.address != null) {
                        // Request balances
                        var balances = {};

                        var cb = function (balances) {
                            if (balances.balance && balances.owed) {
                                // check that we have both balances
                                sendResponse(balances);
                            }
                        };

                        request(ETH_RPC_SERVER + '/balance/' + req.user.address, function (error, response, body) {
                            if (error) {
                                console.error(error.stack)
                                if (!resSent) {
                                    res.status(500).send(error.toString());
                                    resSent = true;
                                }
                            } else {
                                var res_ = JSON.parse(body);
                                balances.balance = res_.balance;
                                cb(balances);
                            }
                        });

                        request(ETH_RPC_SERVER + '/owed/' + req.user.address, function (error, response, body) {
                            if (error) {
                                console.error(error.stack)
                                if (!resSent) {
                                    res.status(500).send(error.toString());
                                    resSent = true;
                                }
                            }
                            else {
                                var res_ = JSON.parse(body);
                                balances.owed = res_.balance;
                                cb(balances);
                            }
                        });
                    } 
                    else sendResponse(null);
                }
                else{
                    res.status(503);
                    res.render('error503', {    new_mess:req.session.new_message,
                                                showLogout : req.session.links_flag, 
                                                isAdmin: is_admin(req)
                                            });
                }
            });
        }
        else {
            res.status(404);
            res.render('error404',{ showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });

    // =====================================
    // API 2 ===============================
    // =====================================

    app.post('/n33dMoarrOnionz', function (req, res) {
        if( req.isAuthenticated() ){
            //вывод на экран
            connection.query("SELECT hostname FROM " + dbconfig.user_info_quest_table + " WHERE username='"+req.user.username+"'", function (err, rows) {
                res.render('final.ejs', {   new_mess:req.session.new_message,
                                            hname: rows[0].hostname?rows[0].hostname:'nothing'
                                        });
            });

        }
        else res.redirect('/');
    });

    // =====================================
    // ADMIN ===============================
    // =====================================

    app.get('/c0n7r9ll_4ll_h1n75', function (req, res) {
        if(is_admin(req)){
            var quests_dyn_conf;
            connection.query("SELECT quests FROM " + dbconfig.hints_table , function (err, quests) {
                quests_dyn_conf = JSON.parse(quests[0].quests);
                
                var captcha = svgCaptcha.create(options);
                req.session.captcha = captcha.text;
                res.render('hints', {    new_mess:req.session.new_message,
                                        cap: captcha.data,
                                        errMess: '' ,
                                        goodMess: '' ,
                                        showLogout : req.session.links_flag,
                                        isAdmin: is_admin(req),
                                        quests_obj: quests_dyn_conf
                                    });
            });
        }
       else {
            res.status(404);
            res.render('error404',{ showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });
    
    app.post('/c0n7r9ll_4ll_h1n75', function (req, res) {
        if(is_admin(req)){
            if(req.body.captcha && req.body.captcha === req.session.captcha){
                var quests_dyn_conf;
                connection.query("SELECT quests FROM " + dbconfig.hints_table , function (err, quests) {
                    quests_dyn_conf = JSON.parse(quests[0].quests);
                    
                    // Reset all to null
                    for (var quest_n in quests_dyn_conf){
                        for(var hint in quests_dyn_conf[quest_n].hints){
                            quests_dyn_conf[quest_n].hints[hint].state = false;
                        }
                    }
                    // Update by new values
                    for(var i in req.body){
                        if(i !== "captcha"){
                            let result_ = req.body[i];
                            if(typeof req.body[i] !== "object") result_ = req.body[i].split(' '); // string to array
                            result_.forEach(function(item){
                                console.log(item + " ");
                                quests_dyn_conf[i].hints[item].state = true;
                            });
                        }
                    }
  
                    connection.query("UPDATE " + dbconfig.hints_table + " SET quests='"+JSON.stringify(quests_dyn_conf)+"' WHERE id='1'");
                    var captcha = svgCaptcha.create(options);
                    req.session.captcha = captcha.text;
                    res.render('hints', {    new_mess:req.session.new_message,
                                        cap: captcha.data,
                                        errMess: '' ,
                                        goodMess: 'Сохранено.' ,
                                        showLogout : req.session.links_flag,
                                        isAdmin: is_admin(req),
                                        quests_obj: quests_dyn_conf
                                    });
                });
            }
            else {
                var captcha = svgCaptcha.create(options);
                req.session.captcha = captcha.text;
                res.render('hints', {    new_mess:req.session.new_message,
                                    cap: captcha.data,
                                    errMess: 'Проверьте введенные данные' ,
                                    goodMess: '' ,
                                    showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    quests_obj: quests_conf
                                });
            }
        }
       else {
            res.status(404);
            res.render('error404',{ showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });
    
    app.get('/news_f0r_4dm1n', function (req, res) {
        if(is_admin(req)){
            var captcha = svgCaptcha.create(options);
            req.session.captcha = captcha.text;
            res.render('news', {    new_mess:req.session.new_message,
                                    cap: captcha.data,
                                    errMess: '' ,
                                    goodMess: '' ,
                                    showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req)
                                });
        }
       else {
            res.status(404);
            res.render('error404',{ showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });

    app.post('/news_f0r_4dm1n', function (req, res) {
        if(is_admin(req)){
            if(req.body.captcha && req.body.captcha === req.session.captcha &&
                req.body.newstext && req.body.newsprev && req.body.newsname){
                connection.query("\
                    INSERT INTO " + dbconfig.news_table + " ( \
                        `news_name`, \
                        `news_prev`, \
                        `news_text` \
                    )" + ' VALUES (\''+req.body.newsname+'\', \''+
                    req.body.newsprev +'\', \''+ req.body.newstext +'\')',function (err, rows) {
                    connection.query("SELECT quests_news FROM " + dbconfig.user_info_quest_table , function (err, rows) {
                        rows.forEach(function(p_row){
                            var old_news = JSON.parse(p_row.quests_news); // в объект
                            old_news.news[req.body.newsname] = 1; // добавили значение (или перезаписали существующее)
                            var res = JSON.stringify(old_news); // обратно в строку

                            // Обновляем
                            connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET quests_news='"+res+"' WHERE quests_news='"+p_row.quests_news+"'");
                        });
                        // чекаем для всех новость или только для залогиненых
                        if (req.body.for_all) { // для всех
                            connection.query("SELECT news FROM " + dbconfig.public_news_table, function (err, rows) {
                                var old_news = JSON.parse(rows[0].news); // news for everyone
                                old_news[req.body.newsname] = 1;
                                var res = JSON.stringify(old_news); // обратно в строку
                                // Обновляем
                                connection.query("UPDATE " + dbconfig.public_news_table+" SET news='"+res+"' WHERE news='"+rows[0].news+"'");
                            });
                        }
                        var captcha = svgCaptcha.create(options);
                        req.session.captcha = captcha.text;
                        res.render('news', {    new_mess:req.session.new_message,
                                                cap: captcha.data,
                                                errMess: '' ,
                                                goodMess: 'Новость добавлена' ,
                                                showLogout : req.session.links_flag,
                                                isAdmin: is_admin(req)
                                            });
                    });
                });
            }
            else {
                var captcha = svgCaptcha.create(options);
                req.session.captcha = captcha.text;
                res.render('news', {    new_mess:req.session.new_message,
                                        cap: captcha.data,
                                        errMess: 'Проверьте введенные данные' ,
                                        showLogout : req.session.links_flag,
                                        isAdmin: is_admin(req)
                                    });
            }
        }
       else {
            res.status(404);
            res.render('error404',{ showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });

    app.get('/quests_t0p_t4bl3', function (req, res) {
        if(is_admin(req)){
            connection.query("SELECT * FROM " + dbconfig.user_info_quest_table , function (err, rows) {
                if (err) res.render('quest', {  new_mess:req.session.new_message,
                                                data: '',
                                                errMess: "Что-то пошло не так..." ,
                                                showLogout : req.session.links_flag,
                                                isAdmin: is_admin(req)
                                            });
                else res.render('quest', {  new_mess:req.session.new_message,
                                            data: rows,
                                            errMess: '' ,
                                            showLogout : req.session.links_flag,
                                            isAdmin: is_admin(req),
                                            quests_numb: passport.quests_number
                                        });
            });
        }
       else {
            res.status(404);
            res.render('error404',{ showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });
    
    app.get('/super_t0p_t4bl3', function (req, res) {
        if(is_admin(req)){
            console.log("[ADMIN] req to bd: start");
            connection.query("SELECT * FROM " + dbconfig.quests_top_table + " ORDER BY quests_numb DESC,points DESC,datetime", function (err, rows) {
                if (err) {
                    console.log("[ADMIN] req to bd: error = " + err);
                    res.render('super_top_table', {  new_mess:req.session.new_message,
                                                data: '',
                                                errMess: "Что-то пошло не так..." ,
                                                showLogout : req.session.links_flag,
                                                isAdmin: is_admin(req)
                                            });
                }
                else {
                    console.log("[ADMIN] req to bd: success");
                    res.render('super_top_table', {  new_mess:req.session.new_message,
                                            data: rows,
                                            errMess: '' ,
                                            showLogout : req.session.links_flag,
                                            isAdmin: is_admin(req),
                                            quests_numb: passport.quests_number
                                        });
                }
            });
        }
       else {
            res.status(404);
            res.render('error404',{ showLogout : req.session.links_flag,
                                    isAdmin: is_admin(req),
                                    images: image_filter(req),
                                    ads_location:req.session.ads_location,
                                    slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                  });
        }
    });

    function is_admin(req) {
        return req.isAuthenticated() && bcrypt.compareSync(req.user.username,
            '$2a$10$ipoYb7RxagDyMXc3nI5hkeLKX09I02PAvUgR8Su8cBCfMCX.vpuvG');
    }
    
    // =====================================
    // CHAT BOT  ===========================
    // =====================================
    app.get('/chat', function(req, res) {
        if(req.isAuthenticated()){
            req.session.new_message = false;
            res.render('chat.ejs', {    new_mess:req.session.new_message,
                                        showLogout : req.session.links_flag,
                                        username: req.user.username,
                                        show_ads:true,
                                        isAdmin: is_admin(req),
                                        images: image_filter(req),
                                        ads_location:req.session.ads_location,
                                        slide_ads: typeof req.session.slide_ads === "undefined" ? slide_ads : req.session.slide_ads
                                    });
        }
        else res.redirect('/');
    });
	
	// Bot
	function send_hello(b_socket, old_quests, u_obj, to_all) {
		var person = "boss";
		var cur_quest = "";
		var mess;
		// Seventh quest is completed
		if(old_quests.quests.quest_7.current_progress === old_quests.quests.quest_7.progress_max){
			person = "elephant";
			cur_quest = "quest_7";
			u_obj.end = true;
			mess = quests_conf.quest_7.success_text;
		}

		// Sixth quest is completed
		else if(old_quests.quests.quest_6.current_progress === old_quests.quests.quest_6.progress_max){
			person = "elephant";
			cur_quest = "quest_6";
			mess = quests_conf.quest_6.success_text;
		}
		// Fifth quest is completed
		else if(old_quests.quests.quest_5.current_progress === old_quests.quests.quest_5.progress_max){
			person = "elephant";
			cur_quest = "quest_6";
			mess = quests_conf.quest_5.success_text;
		}
		// Fourth quest is completed
		else if(old_quests.quests.quest_4.current_progress === old_quests.quests.quest_4.progress_max){
			person = "elephant";
			cur_quest = "quest_5";
			mess = quests_conf.quest_4.success_text;
		}
		// Third quest is completed
		else if(old_quests.quests.quest_3.current_progress === old_quests.quests.quest_3.progress_max){
			person = "elephant";
			cur_quest = "quest_4";
			mess = quests_conf.quest_3.success_text;
		}
		// Second quest is completed
		else if(old_quests.quests.quest_2.current_progress === old_quests.quests.quest_2.progress_max){
			person = "boss";
			cur_quest = "quest_3";
			mess = quests_conf.quest_2.success_text;
		}
		// First quest is completed, but second - not
		else if(old_quests.quests.quest_1.current_progress === old_quests.quests.quest_1.progress_max){
			person = "boss";
			cur_quest = "quest_2";
			mess = quests_conf.quest_1.success_text;
		}
		// No one quest is not completed
		else {
			person = "boss";
			cur_quest = "quest_1";
			mess = first_msg_conf.first_msg_when_register;
		}
		if(to_all){
			u_obj.sockets.forEach((value, valueAgain, soc_set) => {
				for(var i in mess) {
					let filenames = map_user_filenames.get(value.request.session.username__);
					let filename4 = "";
					let filename5 = "";
					if (filenames !== undefined) {
						filename4 = filenames.quest_4;
						filename5 = filenames.quest_5;
					}
					value.emit('fromServer', { 
						server: mess[i].replace("{username}", value.request.session.username__).replace("{url_vinograd_disk}", "https://yo-corp.ru/drive/" + filename4).replace("{url_medved_apk}", "https://yo-corp.ru/drive/" + filename5), person: person
						});
				}
			});
		}
		else{
			for(var i in mess){
				let filenames = map_user_filenames.get(b_socket.request.session.username__);
				let filename4 = "";
				let filename5 = "";
				if (filenames !== undefined) {
					filename4 = filenames.quest_4;
					filename5 = filenames.quest_5;
				}
				b_socket.emit('fromServer', { 
						server: mess[i].replace("{username}", b_socket.request.session.username__).replace("{url_vinograd_disk}", "https://yo-corp.ru/drive/" + filename4).replace("{url_medved_apk}", "https://yo-corp.ru/drive/" + filename5), person: person
					});
			}
		}
		
		u_obj.person = person;
		u_obj.cur_quest = cur_quest;
		return u_obj;
    }
	
	function send_answer(answer, u_obj, b_socket, msg, soc_set, data){
		// Parsing of user's input
		if(answer){
			soc_set.forEach((value, valueAgain, soc_set) => {
				for(var i in answer){
					value.emit('fromServer', { server: answer[i], person: u_obj.person});
				}
			});
		}
		else if (!msg){
			api.getRes(data.client, md5(b_socket.request.session.username__ + USER_SALT)).then(function(res){
				soc_set.forEach((value, valueAgain, soc_set) => {
					value.emit('fromServer', { server: res , person: u_obj.person});
				});
			});
		}
	}
	
	app.io.on('connection', function (b_socket) {
        if(typeof b_socket.request.session.username__ !== "undefined" && 
            typeof b_socket.request.session.links_flag !== "undefined"){
			console.log('User "'+b_socket.request.session.username__+'" connected to chat-bot');
			
			var u_obj = soc_map.get(b_socket.request.session.username__);
			u_obj = u_obj?u_obj:{};
			var soc_set = u_obj.sockets;
			if(!soc_set){
				soc_set = new Set();
				console.log("New set of sockets for user " + b_socket.request.session.username__ + " created!");
			}
			soc_set.add(b_socket);
			u_obj.sockets = soc_set;
			u_obj.current_progress = u_obj.current_progress?u_obj.current_progress:0;
			

			
            // Getting quests info
            let old_quests;
			console.log("[CHAT BOT] (first msg) req to bd: start");
            connection.query("SELECT quests_news FROM " + dbconfig.user_info_quest_table + " WHERE username='"+b_socket.request.session.username__+"'", function (err, rows) {
				if(!err){
					console.log("[CHAT BOT] (first msg) req to bd: finished");
					try {
						old_quests = JSON.parse(rows[0].quests_news); // to object
						
						///////////////////////
						// Send "Hello message"
						///////////////////////

						u_obj = send_hello(b_socket, old_quests, u_obj, false);
						soc_map.set(b_socket.request.session.username__, u_obj);

						///////////////////////
					} catch(e) {
						console.log("[CHAT BOT] (first msg) ERROR " + e);
					}
				}
				else console.log("[CHAT BOT] (first msg) req to bd: ERROR: " + err);
            });

            b_socket.on('fromClient', function (data) {
				var old_u = u_obj;
				u_obj = soc_map.get(b_socket.request.session.username__);
				u_obj = u_obj?u_obj:old_u;
				
                var answer;
                var msg;

                ///////////////////////
                // Parsing of user's answer and giving a result
                ///////////////////////

				if(parse_hex_quest5(data.client, "7627533397a8d226976f9822da65ec6c4dd23fada94774fc50ca5aaa7f6b7561")){
					console.log("[CHAT BOT] (quest_7) req to bd: start; user: " + b_socket.request.session.username__);
					connection.query("SELECT quests_news FROM " + dbconfig.user_info_quest_table + " WHERE username='"+b_socket.request.session.username__+"'", function (err, rows) {
						if(!err){
							console.log("[CHAT BOT] (quest_7) req to bd: finished; user: " + b_socket.request.session.username__);
							old_quests = JSON.parse(rows[0].quests_news); // to object

							if(old_quests.quests.quest_7.current_progress !== old_quests.quests.quest_7.progress_max){
								answer = quests_conf.quest_7.success_text;
								// in case of user can success send 
								// some quest and next quest after that
								// Update DB
								old_quests.quests.quest_7.current_progress = old_quests.quests.quest_7.progress_max;
								if(old_quests.news.private_news_7) old_quests.news.private_news_7 = 1;
								var fin_res = JSON.stringify(old_quests); // return to string
								// Update
								console.log("[CHAT BOT] (quest_7) req to update bd: start; user: " + b_socket.request.session.username__);
								connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET quests_news='"+fin_res+"' WHERE username='"+b_socket.request.session.username__+"'", function (err, rows) {
									if(!err) console.log("[CHAT BOT] (quest_7) req to update bd: finished; user: " + b_socket.request.session.username__);
									else console.log("[CHAT BOT] (quest_7) req to update bd: ERROR; user: " + b_socket.request.session.username__ + " " + err);
								});
                                
                                update_top_table(b_socket.request.session.username__, "7", 0.0); // 7 = number of finished quests
                        
								console.log("+++++++++++++++++++++++++++++\nUser " + b_socket.request.session.username__ + " finished quest_7! \n+++++++++++++++++++++++++++++");
								u_obj.current_progress = 0;
								u_obj.end = true;
								u_obj.cur_quest = "quest_7";
								soc_map.set(b_socket.request.session.username__, u_obj);
							}
							else{
								answer = quests_conf.quest_7.success_text;
							}
						}
						else console.log("[CHAT BOT] (quest_7) req to bd: ERROR; user: " + b_socket.request.session.username__+ " " + err);
						send_answer(answer, u_obj, b_socket, msg, soc_set,data);
					});
				}
				else if(parse_hex_quest5(data.client, "9af45880a1ad9222479a77981a5ada7b9f21218c")){
					console.log("[CHAT BOT] (quest_5) req to bd: start; user: " + b_socket.request.session.username__);
					connection.query("SELECT quests_news FROM " + dbconfig.user_info_quest_table + " WHERE username='"+b_socket.request.session.username__+"'", function (err, rows) {
						if(!err){
							console.log("[CHAT BOT] (quest_5) req to bd: finished; user: " + b_socket.request.session.username__);
							old_quests = JSON.parse(rows[0].quests_news); // to object

							if(old_quests.quests.quest_5.current_progress !== old_quests.quests.quest_5.progress_max){
								answer = quests_conf.quest_5.success_text;
								// in case of user can success send 
								// some quest and next quest after that
								// Update DB
								old_quests.quests.quest_5.current_progress = old_quests.quests.quest_5.progress_max;
								if(old_quests.news.private_news_5) old_quests.news.private_news_5 = 1;
								var fin_res = JSON.stringify(old_quests); // return to string
								// Update
								console.log("[CHAT BOT] (quest_5) req to update bd: start; user: " + b_socket.request.session.username__);
								connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET quests_news='"+fin_res+"' WHERE username='"+b_socket.request.session.username__+"'", function (err, rows) {
									if(!err) console.log("[CHAT BOT] (quest_5) req to update bd: finished; user: " + b_socket.request.session.username__);
									else console.log("[CHAT BOT] (quest_5) req to update bd: ERROR; user: " + b_socket.request.session.username__ + " " + err);
								});

                                update_top_table(b_socket.request.session.username__, "5", 0.0); // 1 = number of finished quests
                        
								console.log("+++++++++++++++++++++++++++++\nUser " + b_socket.request.session.username__ + " finished quest_5! \n+++++++++++++++++++++++++++++");
								u_obj.current_progress = 0;
								u_obj.cur_quest = "quest_6";
								soc_map.set(b_socket.request.session.username__, u_obj);
							}
							else{
								answer = quests_conf.quest_5.success_text;
							}
						}
						else console.log("[CHAT BOT] (quest_5) req to bd: ERROR; user: " + b_socket.request.session.username__+ " " + err);
						send_answer(answer, u_obj, b_socket, msg, soc_set,data);
					});
				}
                else if(parse_word(data.client, "d.vinograd_q")){
                    console.log("[CHAT BOT] (quest_3) req to bd: start; user: " + b_socket.request.session.username__);
                    connection.query("SELECT quests_news FROM " + dbconfig.user_info_quest_table + " WHERE username='"+b_socket.request.session.username__+"'", function (err, rows) {
                        if(!err){
                            console.log("[CHAT BOT] (quest_3) req to bd: finished; user: " + b_socket.request.session.username__);
                            old_quests = JSON.parse(rows[0].quests_news); // to object

                            if(old_quests.quests.quest_3.current_progress !== old_quests.quests.quest_3.progress_max){
                                answer = quests_conf.quest_3.success_text;
                                // in case of user can success send 
                                // some quest and next quest after that
                                // Update DB
                                old_quests.quests.quest_3.current_progress = old_quests.quests.quest_3.progress_max;
                                if(old_quests.news.private_news_3) old_quests.news.private_news_3 = 1;
                                var fin_res = JSON.stringify(old_quests); // return to string
                                // Update
                                console.log("[CHAT BOT] (quest_3) req to update bd: start; user: " + b_socket.request.session.username__);
                                connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET quests_news='"+fin_res+"' WHERE username='"+b_socket.request.session.username__+"'", function (err, rows) {
                                    if(!err) console.log("[CHAT BOT] (quest_3) req to update bd: finished; user: " + b_socket.request.session.username__);
                                    else console.log("[CHAT BOT] (quest_3) req to update bd: ERROR; user: " + b_socket.request.session.username__ + " " + err);
                                });

                                update_top_table(b_socket.request.session.username__, "3", 0.0); // 1 = number of finished quests
                            
                                console.log("+++++++++++++++++++++++++++++\nUser " + b_socket.request.session.username__ + " finished quest_3! \n+++++++++++++++++++++++++++++");
                                u_obj.current_progress = 0;
                                u_obj.cur_quest = "quest_4";
                                soc_map.set(b_socket.request.session.username__, u_obj);
                            }
                            else{
                                answer = quests_conf.quest_3.success_text;
                            }
                        }
                        else console.log("[CHAT BOT] (quest_3) req to bd: ERROR; user: " + b_socket.request.session.username__+ " " + err);
                        send_answer(answer, u_obj, b_socket, msg, soc_set,data);
                    });
				}
				else if(parse_word(data.client, "подсказк")){ // а and у characters included too (word + "[ .,!?ау])
					let hint_conf;
					console.log("[CHAT BOT] (hint) req to bd: start; user: " + b_socket.request.session.username__);
					connection.query("SELECT quests FROM " + dbconfig.hints_table + " WHERE id='1'", function (err, rows) {
						if(!err){
							console.log("[CHAT BOT] (hint) req to bd: finished; user: " + b_socket.request.session.username__);
							hint_conf = JSON.parse(rows[0].quests); // to object
							
							// check progress which is needed
							let cur_progr = u_obj.current_progress + 1;
							let hint_state = false;
							if(hint_conf[u_obj.cur_quest].hints && hint_conf[u_obj.cur_quest].hints["hint_prog_" + cur_progr])
								hint_state = hint_conf[u_obj.cur_quest].hints["hint_prog_" + cur_progr].state;
							
							if(hint_state) {
								msg = quests_conf[u_obj.cur_quest].hints["hint_prog_" + cur_progr].text;
								msg = msg?msg:quests_conf[u_obj.cur_quest].hints["hint_prog_1"].text;
								msg = msg?msg:"Куда уж проще то?! Думай...";
							}
							else msg = "Попробуй спросить позднее, а пока подумай своей головой...";
							
							soc_set.forEach((value, valueAgain, soc_set) => {
								value.emit('fromServer', { server: msg, person: u_obj.person});
							});
						}
						else console.log("[CHAT BOT] (hint) req to bd: ERROR; user: " + b_socket.request.session.username__+ " " + err);
						send_answer(answer, u_obj, b_socket, msg, soc_set,data);
					});
				}
				else if(parse_word(data.client, "слон")){
					if(old_quests.quests.quest_1.current_progress !== old_quests.quests.quest_1.progress_max){
						// in case of user can success send 
						// some quest and next quest after that
						
						answer = quests_conf.quest_1.success_text;
						// Update DB
						old_quests.quests.quest_1.current_progress = old_quests.quests.quest_1.progress_max;
						if(old_quests.news.private_news_1) old_quests.news.private_news_1 = 1;
						var fin_res = JSON.stringify(old_quests); // return to string
						// Update
						console.log("[CHAT BOT] (quest_1) req to update bd: start; user: " + b_socket.request.session.username__);
						connection.query("UPDATE " + dbconfig.user_info_quest_table + " SET quests_news='"+fin_res+"' WHERE username='"+b_socket.request.session.username__+"'", function (err, rows) {
							if(!err) console.log("[CHAT BOT] (quest_1) req to update bd: finished; user: " + b_socket.request.session.username__);
							else console.log("[CHAT BOT] (quest_1) req to update bd: ERROR; user: " + b_socket.request.session.username__ + " " + err);
						});

                        update_top_table(b_socket.request.session.username__, "1", 0.0); // 1 = number of finished quests
                        
						console.log("+++++++++++++++++++++++++++++\nUser " + b_socket.request.session.username__ + " finished quest_1! \n+++++++++++++++++++++++++++++");
						u_obj.current_progress = 0;
						u_obj.cur_quest = "quest_2";
						soc_map.set(b_socket.request.session.username__, u_obj);
					}
					else{
						answer = quests_conf.quest_1.success_text;
					}
					send_answer(answer, u_obj, b_socket, msg, soc_set,data);
				}
				else send_answer(answer, u_obj, b_socket, msg, soc_set,data);
            });
        }
		b_socket.on('disconnect', function() {
			console.log('User '+ b_socket.request.session.username__ +' disconnected from chat-bot!');
			if(soc_set){
				soc_set.delete(b_socket);
				u_obj.sockets = soc_set;
				soc_map.set(b_socket.request.session.username__, u_obj);
			}
		});
    });
};
