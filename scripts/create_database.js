var mysql = require('mysql');
var dbconfig = require('../config/database');
var pub_news_config = require('../config/news');
var priv_news_config = require('../config/private_news');
var quests_conf = require('../config/quests');

var connection = mysql.createConnection(dbconfig.connection);

connection.query('CREATE DATABASE IF NOT EXISTS ' + dbconfig.database + ' CHARACTER SET utf8 COLLATE utf8_general_ci');

// Create table for quests_conf
connection.query('\
CREATE TABLE IF NOT EXISTS `' + dbconfig.database + '`.`' + dbconfig.hints_table + '` ( \
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT, \
    `quests` TEXT NOT NULL, \
        PRIMARY KEY (`id`), \
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) \
)');

// Create table for login - password
connection.query('\
CREATE TABLE IF NOT EXISTS `' + dbconfig.database + '`.`' + dbconfig.users_table + '` ( \
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT, \
    `username` VARCHAR(60) NOT NULL, \
    `password` CHAR(60) NOT NULL, \
    `address` CHAR(42) NULL DEFAULT NULL, \
        PRIMARY KEY (`id`), \
    UNIQUE INDEX `id_UNIQUE` (`id` ASC), \
    UNIQUE INDEX `username_UNIQUE` (`username` ASC) \
)');

// Create table for news
connection.query('\
CREATE TABLE IF NOT EXISTS `' + dbconfig.database + '`.`' + dbconfig.news_table + '` ( \
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT, \
    `news_name` VARCHAR(20) NOT NULL, \
    `news_text` LONGTEXT NOT NULL, \
    `news_prev` TEXT NOT NULL, \
        PRIMARY KEY (`id`), \
    UNIQUE INDEX `id_UNIQUE` (`id` ASC), \
    UNIQUE INDEX `news_name_UNIQUE` (`news_name` ASC) \
)');

// Create table for quests
connection.query('\
CREATE TABLE IF NOT EXISTS `' + dbconfig.database + '`.`' + dbconfig.user_info_quest_table + '` ( \
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT, \
    `username` VARCHAR(60) NOT NULL, \
    `quests_news` TEXT NOT NULL, \
    `dockerID` TEXT(1024),\
    `filename4` TEXT(1024),\
    `filename5` TEXT(1024),\
        PRIMARY KEY (`id`), \
    UNIQUE INDEX `id_UNIQUE` (`id` ASC), \
    UNIQUE INDEX `username_UNIQUE` (`username` ASC) \
)');

// Create table for top_quests
connection.query('\
CREATE TABLE IF NOT EXISTS `' + dbconfig.database + '`.`' + dbconfig.quests_top_table + '` ( \
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT, \
    `username` VARCHAR(60) NOT NULL, \
    `quests_numb` INT UNSIGNED, \
    `points` FLOAT,\
    `datetime` DATETIME,\
        PRIMARY KEY (`id`), \
    UNIQUE INDEX `id_UNIQUE` (`id` ASC), \
    UNIQUE INDEX `username_UNIQUE` (`username` ASC) \
)');

// Create table for new news
connection.query('\
CREATE TABLE IF NOT EXISTS `' + dbconfig.database + '`.`' + dbconfig.public_news_table + '` ( \
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT, \
    `news` TEXT NOT NULL, \
        PRIMARY KEY (`id`), \
    UNIQUE INDEX `id_UNIQUE` (`id` ASC) \
)');

// Insert default news
var news_list = '';
for (key in pub_news_config) { news_list = '"' + key + '":' + pub_news_config[key].default_value + ',' + news_list; }
news_list = news_list.substring(",", news_list.length - 1); // remove last ","

var insertQuery = "INSERT INTO `"+dbconfig.database+"`.`" + dbconfig.public_news_table +"` ( news ) values (?)";
connection.query(insertQuery, ['{' + news_list + '}']);

// Insert default quests
for(var i in quests_conf){
    quests_conf[i].success_text = '';
    for(var j in quests_conf[i].hints){
        quests_conf[i].hints[j].text = '';
    }
}
var quests_list = JSON.stringify(quests_conf);// to object

insertQuery = "INSERT INTO `"+dbconfig.database+"`.`" + dbconfig.hints_table +"` ( quests ) values (?)";
connection.query(insertQuery, [quests_list]);

console.log('Success: Database Created!');

// Adding news
for (key in pub_news_config) {
    connection.query('\
    INSERT INTO `' + dbconfig.database + '`.`' + dbconfig.news_table + '` ( \
        `news_name`, \
        `news_prev`, \
        `news_text` \
    )' + ' VALUES (\''+key+'\', \''+ pub_news_config[key].news_prev +'\', \''+ pub_news_config[key].news_text +'\')');
}
for (key in priv_news_config) {
    connection.query('\
    INSERT INTO `' + dbconfig.database + '`.`' + dbconfig.news_table + '` ( \
        `news_name`, \
        `news_prev`, \
        `news_text` \
    )' + ' VALUES (\''+key+'\', \''+ priv_news_config[key].news_prev +'\', \''+ priv_news_config[key].news_text +'\')');
}

connection.end();

console.log('Success: News is added!');
