// server.js

// set up ======================================================================
// get all the tools we need
const express = require('express');
// import necessary modules for Passport
const passport = require('passport');
const session = require('express-session');

var conf_settings = require('./config/const_settings');

var redisStore = require('connect-redis')(session);

const app = express();
app.io = require('socket.io')();

var favicon = require('serve-favicon');

const uuidv4 = require('uuid/v4');
var sharedsession = require("express-socket.io-session");

var busboy = require('connect-busboy');
app.use(busboy({ limits: { files: 1, fileSize: 50000, fieldSize: 50000, fields: 5 }  }));

var sessionStore = new redisStore({ host : conf_settings.redis_server_host, 
                                    port : conf_settings.redis_server_port });
var session_options = {
    genid: function(req) {
        return uuidv4();
    },
    secret: conf_settings.session_middleware_secret,
    resave: true,
    saveUninitialized: true,
    cookie: {
        httpOnly: true
    },
    key: conf_settings.session_cookie_name,
};
if(conf_settings.use_redis_server === 1) session_options.store = sessionStore;
if(conf_settings.send_cookie_via_https === 1) session_options.cookie.secure = true; // cookie send via https
var sessionMiddleware = session(session_options);
// required for passport
app.use(sessionMiddleware); // session secret

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var path = require('path');
var port = process.env.PORT || 8080;
var flash = require('connect-flash');

app.use(express.static(path.join(__dirname, 'public')));
app.use('/static', express.static(__dirname + '/public/images/ads/'));
app.use('/static', express.static(__dirname + '/public/quest_1_files/'));
app.use('/drive', express.static(__dirname + '/public/quest_files/'));
// security ====================================================================
var helmet = require('helmet');
app.use(helmet());

app.disable('x-powered-by');
// Sets "X-Content-Type-Options: nosniff".
app.use(helmet.noSniff())
// Sets "X-XSS-Protection: 1; mode=block".
app.use(helmet.xssFilter())
app.use(helmet.frameguard({ action: 'sameorigin' }))
// Sets "X-DNS-Prefetch-Control: off".
app.use(helmet.dnsPrefetchControl())

// configuration ===============================================================
// connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());

app.set('view engine', 'ejs'); // set up ejs for templating
app.set('views', __dirname + '/views');

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
var mysql = require('mysql');
var dbconfig = require('./config/database');
var connection = mysql.createConnection(dbconfig.connection);
require('./app/routes.js')(app, passport, connection, sessionMiddleware); // load our routes and pass in our app and fully configured passport

app.use(favicon(__dirname + '/public/images/favicon.ico'));

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.status(404).render('error404',{ showLogout : req.session.links_flag,
                                    ads_location:req.session.ads_location
                                  });
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// launch ======================================================================
var server = app.listen(port);
app.io.attach(server);

// bot =========================================================================
//require('./config/chat_bot/sock')(server, sessionMiddleware, connection);

console.log('The magic happens on port ' + port);
