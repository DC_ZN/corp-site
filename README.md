## Instructions for dev host (without Docker)

1. Run MySql (I use XAMPP for example)
1. Edit the database configuration: `config/database.js` (set connection's user, password)
1. Create the database schema: `node scripts/create_database.js`
1. Install all dependencies
1. Edit `config/const_settings.js` as you need
1. Edit address in `public/js/convo.js`
1. Install Redis: `https://redis.io/topics/quickstart` (if you enabled it)
1. Launch: `node server.js`
1. Visit in your browser at: `http://localhost:8080`

Dependencies:
```
npm install mysql express express-session cookie-parser morgan passport connect-flash passport-local bcrypt-nodejs request svg-captcha express-http-proxy ejs socket.io apiai helmet uuid connect-redis md5 connect-busboy js-sha256 crypto
```

It's need to register admin user with:
```
login: batya8in8a8h0us3
pwd: OH55Alla123S0m31YEAH // can be any
```

### API FOR TOP-TABLE

Just make get-request to `yo-corp.ru/70p_u53r5_74bl3?param=I7_I5_me_g37_t0p_pl5` and an example of answer is:
```json
{
    secret: "h3r3__y0u__g0",
    data: {
        user_name_1: {
            quest_1: {
                current_progress:0,
                progress_max:5
            },
            quest_2: {
                current_progress:0,
                progress_max:5
            }
        }
    }
}
```

### NEW TOP TABLE

logins: batya8in8a8h0us3 and dcnnttestt[a-zA-Z1-9] will be ignored; // ex.: dcnnttestt1, dcnnttestt2, dcnnttestt10, dcnnttestt5555

### DISPLAYING INFORMATION ON TV
1) Download Firefox
2) Go to https://addons.mozilla.org/ru/firefox/addon/tab-rotator/
3) Click Add to Firefox
4) Open two tabs: http://top.defcon-nn.ru/ and local file qr_page.html (you can find it in the Telegram by tag #qr_page)
5) Right click to Tav-rotator addon - Manage Extension
6) Set Rotation time (30 sec) and fill Reload Tabs check box
7) Click save
8) Make sure that only two tabs are open (http://top.defcon-nn.ru/ and local file qr_page.html)
9) Left click to Tab-rotator addon (to Start)
10) Tap f11 key and give Tab-rotator update all tabs, so they will fill all page
11) To stop Tab-rotator tap f11 again and Left click to Tab-rotator addon
