var apiai = require('apiai');

// read the api.ai docs : https://api.ai/docs/

//Enter your API Key
var APP = apiai("9c60891f82c44e72bdbbe7f45edf2a44");
var SALT_SESSION_ID = "__I7i5s4l7F0rS355i0NOf_u5eR__";

// Function which returns speech from api.ai
var getRes = function(query, sessionId) {
    var request = APP.textRequest(query, {
        sessionId: sessionId
    });
    const responseFromAPI = new Promise(function (resolve, reject) {
        request.on('error', function(error) {
            reject(error);
        });
        request.on('response', function(response) {
            resolve(response.result.fulfillment.speech);
        });
    });
    request.end();
    return responseFromAPI;
};

module.exports = {getRes} 
