// config/news.js
module.exports = {
    // New news on top
    // --- Personal news
    private_news_7: {
        news_prev : '<img align="left" style="margin:10px; border:1px solid #16244d;" src="images/nyse.jpeg" alt="" height="150">' +
        '<h2 class="post-title">\n' +
        '          Паника на бирже из-за вала обнуленных кредитов\n' +
        '          </h2>\n' +
        '          <h3 class="post-subtitle">\n' +
        '          Сегодня на криптовалютной бирже разразилась паника из-за новой акции хакерской группировки "Друзья Чиполлино". \n' +
        '          </h3>' +
        '          </a>\n' +
        '          <p class="post-meta">\n' +
        '          28 июля 2078</p>\n' +
        '          </div>\n' +
        '          <hr>',
        news_text : '<h2 class="section-heading">Паника на бирже из-за вала обнуленных кредитов</h2>'+
        '\n'+
        '<p>Сегодня на криптовалютной бирже разразилась паника из-за новой акции хакерской группировки "Друзья Чиполлино". Группировка опубликовала в Сети сайт, где любой желающий может обнулить свой кредит. По официальному сообщению хакеров, это стало возможным благодаря тому, что они смогли завладеть приватным ключом Ётериума. Естественно, сайт был тут же заблокирован по требованию Ё-корп, а доменное имя разделегировано. К сожалению, это не помогло: хакеры поддерживают сайт в анонимных сетях, где блокировка административными методами невозможна. По сообщению источника в службе безопасности Ё-корп, корпорация готовится принять активные меры с целью вывести сайт из строя, а также обнаружить всех обнуливших кредиты, и привлечь преступников к ответственности по всей строгости корпоративно-уголовного кодекса.\n</p>' +
                    '\n' +
        '               <div style="text-align: center;"> <img class="img-fluid" src="images/nyse.jpeg" alt=""></div>\n' +
        '\n' +
        '<p>Рынок криптовалют ответил на эти события резким падением Ётериума. На данный момент курс приближается к нулевой отметке.</p>'
    },    
    
	private_news_6: {
        news_prev : '<img align="left" style="margin:10px; border:1px solid #16244d;" src="images/credits.jpeg" alt="" height="150">' +
        '<h2 class="post-title">\n' +
        '          Ё-корп делает кредиты ещё доступнее\n' +
        '          </h2>\n' +
        '          <h3 class="post-subtitle">\n' +
        '          Ё-корп объявляет о сезонном снижении ставки по обязательному кредиту для всех клиентов с высоким социальным рейтингом.\n' +
        '          </h3>' +
        '          </a>\n' +
        '          <p class="post-meta">\n' +
        '          28 июля 2078</p>\n' +
        '          </div>\n' +
        '          <hr>',
        news_text : '<h2 class="section-heading">Ё-корп делает кредиты ещё доступнее</h2>'+
		'\n'+
		'<p>Ё-корп объявляет о сезонном снижении ставки по обязательному кредиту для всех клиентов, проявивших благонадежность и набравших высокий рейтинг в системе наблюдения и коррекции поведения.\n</p>' +
					'\n' +
        '                <div style="text-align: center;"><img class="img-fluid" src="images/credits.jpeg" alt=""></div>\n' +
		'\n' +
		'<p>Для клиентов, чьи показатели неблагонадежности переполнились, объявлена амнистия!</p>'
        
    },
    private_news_5: {
        news_prev : '<img align="left" style="margin:10px; border:1px solid #16244d;" src="images/vegrebel.jpeg" alt="" height="150">' +
        '<h2 class="post-title">\n' +
        '          Банда генномодифициованных овощей захватила Центр передовых технологий Ё-корп\n' +
        '          </h2>\n' +
        '          <h3 class="post-subtitle">\n' +
        '          Овощи вышли из повиновения ученых, убили нескольких из них, а остальных взяли в заложники.\n' +
        '          </h3>' +
        '          </a>\n' +
        '          <p class="post-meta">\n' +
        '          28 июля 2078</p>\n' +
        '          </div>\n' +
        '          <hr>',
        news_text : '<h2 class="section-heading">Банда генномодифициованных овощей захватила Центр передовых технологий Ё-корп</h2>'+
        '\n'+
        '<p>По сообщению Службы безопасности Ё-корп, Центр передовых технологий захватила группа генномодифицированных овощей. Инцидент произошел в 13-45 по Московскому времени. Овощи вышли из повиновения ученых, убили нескольких из них, захватили оружие у охранников и держат в заложниках администратора баз данных Центра Владислава Редисенко, а также уборщицу Екатерину Васильеву и младшего научного сотрудника центра Максима Мазура. Похоже, что лидером восставших является гигантский лук. Пока неизвестно о каких-либо требованиях захватчиков. К месту событий выехали отряды полиции и журналисты.\n</p>' +
                    '\n' +
        '                <div style="text-align: center;"><img class="img-fluid" src="images/vegrebel.jpeg" alt=""></div>\n' +
        '\n' +
        '<p>Напомним, что недавно в Лаборатории генной инженерии Ё-корп были созданы элитные виды овощей, призванные продлевать жизнь. По словам специалистов, которым сегодня посчастливилось отсутствовать на работе во время захвата, они не могли и предположить, что их создания обретут разум и будут способны на агрессивные действия.</p>'
        
    },
	private_news_4: {
        news_prev : '<img align="left" style="margin:10px; border:1px solid #16244d;" src="images/grapes.jpeg" alt="" height="150">' +
        '<h2 class="post-title">\n' +
        '          Обезврежен опасный террорист\n' +
        '          </h2>\n' +
        '          <h3 class="post-subtitle">\n' +
        '          Им оказался системный администратор нижегородского ресторана Ё-фуд Денис Васильевич Виноградов. Он воспользовался анонимной сетью и поплатился.\n' +
        '          </h3>' +
        '          </a>\n' +
        '          <p class="post-meta">\n' +
        '          28 июля 2078</p>\n' +
        '          </div>\n' +
        '          <hr>',
        news_text : '<h2 class="section-heading">Обезврежен опасный террорист</h2>'+
		'\n'+
		'<p>Служба безопасности Ё-корп выявила нарушителя Правил пользования Интернетом.\n</p>' +
					'\n' +
        '                <div style="text-align: center;"><img class="img-fluid" src="images/grapes.jpeg" alt=""></div>\n' +
        '\n' +
		'<p>Им оказался системный администратор нижегородского ресторана Ё-фуд Денис Васильевич Виноградов.</p>'+
        '\n' +
		'<p>Как выяснилось, этот опасный преступник, известный под кличкой Vinograd, воспользовалься анонимной сетью. В рамках коррекционной программы Отдел по работе с клиентами Ё-банка провел снижение дебетового баланса Виноградова Д.В., а также установил повышенную процентную ставку по кредиту.</p>'
    },

	private_news_3: {
        news_prev : '<img align="left" style="margin:10px; border:1px solid #16244d;" src="images/y2.jpg" alt="" height="150">' +
        '<h2 class="post-title">\n' +
        '          Опасные преступники-террористы остались без связи\n' +
        '          </h2>\n' +
        '          <h3 class="post-subtitle">\n' +
        '          Служба Ё-корп по коррекции поведения в Интернете совместно с коллегами из материнской United Transnational Megacorp пресекла деятельность подпольного почтового сервера.\n' +
        '          </h3>' +
        '          </a>\n' +
        '          <p class="post-meta">\n' +
        '          28 июля 2078</p>\n' +
        '          </div>\n' +
        '          <hr>',
        news_text : '<h2 class="section-heading">Опасные преступники-террористы остались без связи</h2>'+
		'\n'+
		'' +
					'\n' +
        '                <div style="text-align: center;"><img class="img-fluid" src="images/y2.jpg" alt=""></div>\n' +
        '\n' +
		'<p>Служба Ё-корп по коррекции поведения в Интернете совместно с Red Team из материнской United Transnational Megacorp объявили о пресечении деятельности подпольного почтового сервера.</p>'+
        '\n' +
		'<p>В совместном заявлении подчеркивается, что сервис предоставлял услуги без СМС и регистрации, в том числе в анонимных сетях. Напоминаем, что это является тяжким преступлением, корпоративными правилами предусмотрена ответственность в виде пожизненого отключения от системы финансирования Ётериум.</p>'
        
    },

	private_news_2: {
        news_prev : '<img align="left" style="margin:10px; border:1px solid #16244d;" src="images/ad.jpg" alt="" height="150">' +
        '<h2 class="post-title">\n' +
        '          Раскрыта польза таргетированной рекламы\n' +
        '          </h2>\n' +
        '          <h3 class="post-subtitle">\n' +
        '          Рекламные сети заботятся о нас.\n' +
        '          </h3>' +
        '          </a>\n' +
        '          <p class="post-meta">\n' +
        '          28 июля 2078</p>\n' +
        '          </div>\n' +
        '          <hr>',
        news_text : '<h2 class="section-heading">Раскрыта польза таргетированной рекламы </h2>' + '\n' + 
        '<p>Маркетологи Ё-корп опубликовали результаты масштабного исследования, в котором подтверждается польза тотальной точно таргетированной рекламы для общества.</p>' + '\n' +
        '                <div style="text-align: center;"><img class="img-fluid" src="images/ad.jpg" alt=""></div>\n' +
        
'<p>Исследование проводилось в течение года на 5% аудитории сети Интернет, изучалось влияние рекламны на B2C-компании и непосредственно конечных покупателей. Для того, чтобы предварительно изучить потребности и желания потребителей были применены новейшие разработки Корпорации в области машинного обучения и анализа больших данных. Все участники исследования тщательно изучались с социальной, поведенческой, лингвистической и других точек зрения. Также важным параметром в исследовании являлся социальный рейтинг скрытно изучаемых "подопытных". При этом, по словам экспертов, приватность пользователей не пострадала, ведь все их данные обрабатывались только с помощью искусственного интеллекта.</p>' + '\n' + 

'<p>Для компаний были отмечены такие положительные эффекты: ' + '\n' +
'<ul><li> увеличились доходы сайтов, существующих благодаря рекламе </li>' + '\n' +
'<li>количество лидов на сайтах компаний выросло на 95%;</li>' + '\n' +
'<li>продажи выросли не менее чем на 85%;</lib>' + '\n' +
'<li>позиции в поисковых системах резко возросли у всех рекламодателей, причем, стоит отметить, согласно статистике, главную роль здесь сыграли поведенческие факторы.</li>' + '\n' +

'<p>Однако, кроме прямых выгод для рекламодателей, были и косвенные: выросла удовлетворенность целевой аудитории, а также выросли количественные показатели узнаваемости брендов. Как выяснили учёные, люди, просматривающие точно таргетированную рекламу, не испытывали негативных эмоций, в отличие от тех, кто получал обычную рекламу -- ведь точно таргетированная реклама предвосхищает и формирует все сиюминутые и долгосрочные желания людей.</p>' + '\n' +

'<p>Как сообщают источники в Ё-корп, в ближайшее время практика тотального таргетирования будет введена повсеместно для рекламы бизнеса. Как заявляют в Корпорации, это только первые шаги к настоящей цели - построению более гармоничного и счастливого социума с помощью точечного воздействия позитивного рекламного посыла на каждого отдельного индивидуума, что должно стать главным фактором в формировании его как личности. Также отмечается, что искусственный интеллект будет постоянно изучать аудиторию для перманентного улучшения таргетинга.</p>'        
    },
    
	private_news_1: {
        news_prev : '<img align="left" style="margin:10px; border:1px solid #16244d;" src="images/breech.jpg" alt="" height="150">' +
        '<h2 class="post-title">\n' +
        '          А был ли мальчик? Ё-корп всё отрицает\n' +
        '          </h2>\n' +
        '          <h3 class="post-subtitle">\n' +
        '          По информации сетевого издания "Новости 01101", вчера произошел взлом компании Ё-Новости, и хакеры оставили публичное сообщение.\n' +
        '          </h3>' +
        '          </a>\n' +
        '          <p class="post-meta">\n' +
        '          28 июля 2078</p>\n' +
        '          </div>\n' +
        '          <hr>',
        news_text : '<h2 class="section-heading">А был ли мальчик? Ё-корп всё отрицает</h2>'+
        '\n'+
        '<p>По сообщению сетевого издания "Новости 01101", вчера произошел взлом компании Ё-Новости. И злоумышленниками якобы была опубликована новость, в которой они взяли на себя ответственность за обнуление социального рейтинга главы Отдела банковских задолженностей господина Грейпфрута. Вместо подписи они оставили картинку шахматного слона.\n</p>' +
                    '\n' +
        '                <div style="text-align: center;"><img class="img-fluid" src="images/breech.jpg" alt=""></div>\n' +
        '\n' +
        '<p>Однако, согласно официальному пресс-релизу Ё-корп, главного акционера Ё-Новости, никакого взлома не было, периметр защиты корпорации совершенно нетронут. Кроме того, по последним данным специалистов по информационной безопасности Корпорации, случай с господином Грейпфрутом — это единичный сбой системы. На данный момент последствия сбоя устранены и беспокоиться совершенно не о чем: ваш социальный рейтинг надёжно защищён.</p>'+
        '\n'
    }
};
