// config/passport.js

// load all the things we need
var LocalStrategy = require('passport-local').Strategy;

// load up the user model
var mysql = require('mysql');
var bcrypt = require('bcrypt-nodejs');
var dbconfig = require('./database');
var connection = mysql.createConnection(dbconfig.connection);
var request = require('request');

var priv_news_config = require('./private_news');
var pub_news_config = require('./news');
var quests_config = require('./quests');

connection.query('USE ' + dbconfig.database);
// expose this function to our app using module.exports
module.exports = function(passport) {
    var public_news = '';
    var quests_list = '';

    passport.quests_number = 0;

    for (key in pub_news_config) { public_news = '"' + key + '":1,' + public_news; }
    for (key in quests_config) {
        quests_list = '"' + key + '": {"current_progress":' + quests_config[key].current_progress +
                        ',"progress_max":' + quests_config[key].progress_max +
                        ',"hostname":-1,"ip":-1,"password":-1},' + quests_list;
        passport.quests_number += 1;
    }
    quests_list = quests_list.substring(",", quests_list.length - 1); // remove last ","

    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        connection.query("SELECT * FROM " + dbconfig.users_table + " WHERE id = ? ",[id], function(err, rows){
            done(err, rows[0]);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use(
        'local-signup',
        new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) {
            // check the captcha
            if(req.body.captcha && (req.body.captcha !== req.session.captcha)){
                return done(null, false, req.flash('signupMessage', 'Проверьте капчу!'));
            }
            else if(!(/^[a-z0-9]{4,20}$/.test(username))){
                return done(null, false, req.flash('signupMessage', 'Имя пользователя не соответствует правилам'));
            }
            else if(!(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/.test(password))){
                return done(null, false, req.flash('signupMessage', 'Пароль не соответствует правилам'));
            }
            else {
                // find a user whose email is the same as the forms email
                // we are checking to see if the user trying to login already exists
                connection.query("SELECT * FROM " + dbconfig.users_table + " WHERE username = ?", [username], function (err, rows) {
                    if (err) {
                        console.log("Error when signup (SELECT): " + err);
                        return done(err);
                    }
                    if (rows.length) {
                        return done(null, false, req.flash('signupMessage', 'Это имя уже занято.'));
                    } else {
                        // if there is no user with that username
                        // create the user
                        var newUserMysql = {
                            username: username,
                            password: bcrypt.hashSync(password, null, null)  // use the generateHash function in our user model
                        };

                        var insertQuery = "INSERT INTO " + dbconfig.users_table + " ( username, password) values (?,?)";

                        connection.query(insertQuery, [newUserMysql.username, newUserMysql.password], function (err, rows) {
                            if (err) {
                                console.log("Error when signup (insertQuery): " + err);
                                return done(err);
                            }
                            newUserMysql.id = rows.insertId;

                            var private_news = '';
                            for (key in priv_news_config) {
                                //if admin
                                if(bcrypt.compareSync(username, '$2a$10$ipoYb7RxagDyMXc3nI5hkeLKX09I02PAvUgR8Su8cBCfMCX.vpuvG'))
                                    private_news = '"' + key + '":1,' + private_news;
                                else private_news = '"' + key + '":-1,' + private_news;
                            }
                            private_news = private_news.substring(",", private_news.length - 1); // remove last ","

                            // insert news
                            var quests_req = '{ ' +
                                '"news" : {'  +
                                public_news   + private_news + '}, ' +
                                '"quests" : {' +
                                quests_list + '}}';
                            var insertQuery = "INSERT INTO " + dbconfig.user_info_quest_table + " ( username, quests_news ) values (?,?)";

                            connection.query(insertQuery, [newUserMysql.username, quests_req],function (err, rows) {
                                if (err) {
                                    console.log("Error when signup (insertQuery 2): " + err);
                                    return done(err);
                                }
                                // console.log('Error: ' + err);
                                return done(null, newUserMysql);
                            });
                        });

                        insertQuery = "INSERT INTO " + dbconfig.quests_top_table + " ( username, quests_numb, points, datetime) values (?,?,?,?)";
                        connection.query(insertQuery, [newUserMysql.username, 0, 0.1, new Date()], function (err, rows) {
                            if (err) {
                                console.log("Error when insert into TOP_TABLE: " + err);
                            }
                            else console.log("[SUCCESS] insert into TOP_TABLE");
                        });
                    }
                });
            }
        })
    );

    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'

    passport.use(
        'local-login',
        new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email
            usernameField : 'username',
            passwordField : 'password',
            passReqToCallback : true // allows us to pass back the entire request to the callback
        },
        function(req, username, password, done) { // callback with email and password from our form
            // check the captcha
            if(req.body.captcha && (req.body.captcha !== req.session.captcha)){
                return done(null, false, req.flash('loginMessage', 'Проверьте капчу!'));
            }
            else if(!(/^[a-z0-9]{4,20}$/.test(username))){
                return done(null, false, req.flash('signupMessage', 'Имя пользователя не соответствует правилам'));
            }
            else if(!(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$/.test(password))){
                return done(null, false, req.flash('signupMessage', 'Пароль не соответствует правилам'));
            }
            else {
                connection.query("SELECT * FROM " + dbconfig.users_table + " WHERE username = ?", [username], function (err, rows) {
                    if (err) {
                        console.log("Error when signup (insertQuery 2): " + err);
                        return done(err);
                    }
                    if (!rows.length) {
                        return done(null, false, req.flash('loginMessage', 'Пользователь не найден.')); // req.flash is the way to set flashdata using connect-flash
                    }

                    // if the user is found but the password is wrong
                    if (!bcrypt.compareSync(password, rows[0].password))
                        return done(null, false, req.flash('loginMessage', 'Упс! Не правильный пароль!')); // create the loginMessage and save it to session as flashdata

                    // all is well, return successful user
                    return done(null, rows[0]);
                });
            }
        })
    );
};
